﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Puhk.Models
{
    public partial class Töötaja
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Kohustuslik väli")]
        [StringLength (11, ErrorMessage = "Isikukood ei saa olla pikem kui 11 tähemärki")]
        public string Isikukood { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        [StringLength(50, ErrorMessage = "Nime maksimumpikkus on 50 tähemärki")]
        public string Nimi { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        [StringLength(40, ErrorMessage = "Kontakti maksimumpikkus on 40 tähemärki")]
        public string Kontakt { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name ="Tööle asumise kuupäev")]
        public DateTime TööAlgusKP { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Töösuhte lõppkuupäev")]
        public DateTime? TööLõppKP { get; set; }

        public virtual Osakond Osakond { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        public int OsakondId { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        public Roll TöötajaRoll { get; set; }


        public enum Roll { Firmajuht, Personalijuht, Osakonnajuht, Töötaja }

        public virtual ICollection<Puhkus> Puhkused { get; set; }
    }
}