﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace Puhk.Models
{
    public class PuhkContext : DbContext
    {
        public PuhkContext() : base("PuhkContext")
        {

        }
        public DbSet<Osakond> Osakonnad { get; set; }
        public DbSet<Töötaja> Töötajad { get; set; }
        public DbSet<Puhkus> Puhkused { get; set; }
    }
}