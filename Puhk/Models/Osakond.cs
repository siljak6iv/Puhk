﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Puhk.Models
{
    public class Osakond
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        public string Nimetus { get; set; }

        public virtual ICollection<Töötaja> Töötajad { get; set; }


    }
}