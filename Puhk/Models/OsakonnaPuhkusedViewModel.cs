﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Puhk.Models
{
    public class OsakonnaPuhkusedViewModel
    {
        public string Nimetus { get; set; }
        public List<Puhkus> Puhkused { get; set; }
    }
}