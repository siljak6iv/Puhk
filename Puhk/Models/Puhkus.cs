﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Puhk.Models
{
    public class Puhkus
    {
        
        public int Id { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        public Tüüp PuhkuseTüüp { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Puhkuse algus")]
        public DateTime AlgusKP { get; set; }
        
        [Required(ErrorMessage = "Kohustuslik väli")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Puhkuse lõpp")]
        public DateTime LõppKP { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli")]
        public int TöötajaId { get; set; }

        public virtual Töötaja Töötaja { get; set; }

        public string Staatus { get; set; }

        public enum Tüüp { Tavapuhkus, Lapsehoolduspuhkus, Õppepuhkus }
        

    }
}