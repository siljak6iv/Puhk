﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Puhk.Startup))]
namespace Puhk
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
