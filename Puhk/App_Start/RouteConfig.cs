﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Puhk
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Töötaja",
                url: "Töötaja/Details/{id}",
                defaults: new { controller = "Töötaja", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Osakond",
                url: "Töötaja/Details/{id}",
                defaults: new { controller = "Töötaja", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Firmajuht",
                url: "Töötaja/Details/{id}",
                defaults: new { controller = "Töötaja", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Personalijuht",
                url: "Töötaja/Details/{id}",
                defaults: new { controller = "Töötaja", action = "Details", id = UrlParameter.Optional }
            );
        }
    }
}
