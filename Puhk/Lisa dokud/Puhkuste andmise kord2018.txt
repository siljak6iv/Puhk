Puhkuste andmise kord . // ettevõtte kinnitatud //
1.1 Käesolev puhkuste andmise kord reguleerib töötajatele põhi- ja lisapuhkuse, vanemapuhkuse ja õppepuhkuse andmise põhimõtted ja korra ettevõttes.
1.2 Puhkuse kestust arvutatakse kalendripäevades, mille hulka ei arvata rahvuspüha ja riigipühi.
1.3 Poolte kokkuleppel võib põhipuhkust anda osade kaupa, kusjuures ühe katkematu osa kestus peab olema vähemalt 14 kalendripäeva. 
1.4 Tööandjal on õigus keelduda põhipuhkuse jagamisest lühemaks kui seitsmepäevaseks osaks.
1.5 Tööle asumise kalendriaastal arvutatakse kalendriaastast lühema aja eest põhipuhkust võrdeliselt töötatud ajaga. Töötaja võib nõuda puhkust, kui ta on tööandja juures töötanud vähemalt kuus kuud. 
1.6 Põhipuhkuse nõue aegub ühe aasta jooksul arvates selle kalendriaasta lõppemisest, mille eest puhkust arvestatakse. 
2. Põhipuhkus 
2.1 Põhipuhkuse kestus on 28 kalendripäeva.
2.2 Isikutele, kellele on määratud töövõimetuspension, on ette nähtud pikendatud põhipuhkus 35 kalendripäeva.
2.3 Osalise tööajaga töötaja põhipuhkus on sama kestusega kui täistööajaga töötajal.
2.4 Iga töötatud kalendriaasta eest on töötajal õigus saada põhipuhkust täies ulatuses.
2.5 Põhipuhkus tuleb üldjuhul kasutada kalendriaasta jooksul. Kasutamata puhkuseosa viiakse üle järgmisesse kalendriaastasse. 
3. Puhkuse andmise kord 
3.1 Puhkuste ajakava koostamisel lähtub tööandja töö korraldamise huvidest, arvestades võimaluse korral töötajate soove. 
3.2 Töötaja esitab puhkusesoovi vahetule osakonnajuhile. Vahetu juht esitab kooskõlastatud puhkusesoovid personalijuhile iga aasta 21.märtsiks. 
Puhkusegraafik kinnitatakse Ettevõtte juhi poolt iga aasta 31. Märtsiks.

3.3 Töötaja soovitud ajal on tööandja kohustatud andma puhkust: 
3.3.1 naisele vahetult enne ja pärast rasedus- ja sünnituspuhkust või vahetult pärast lapsehoolduspuhkust
3.3.2 mehele vahetult pärast lapsehoolduspuhkust või naise rasedus- ja sünnituspuhkuse ajal .
3.3.3 vanemale, kes kasvatab kuni seitsmeaastast last 
3.3.4 vanemale, kes kasvatab seitsme- kuni kümneaastast last, -lapse koolivaheajal 
3.3.5 koolikohustuslikule alaealisele – koolivaheajal
3.4 Puhkuste ajakava kinnitab direktor oma käskkirjaga.
3.5 Puhkuste ajakava koostatakse iga kalendriaasta kohta.
3.6 Puhkuste ajakava tehakse töötajatele teatavaks kalendriaasta esimese kvartali jooksul.
3.7 Puhkuste ajakavasse märkimata puhkuse kasutamisest teatab töötaja tööandjale 14 kalendripäeva ette kirjaliku avalduse alusel
3.8 Puhkuste ajakava on õigus muuta tööandja ja töötaja kokkuleppel. Puhkuste ajakava muutmisel määratakse kindlaks uus puhkuse aeg. 
4. Vanemapuhkused 
4.1 Lapsendaja puhkust, isapuhkust, lapsehoolduspuhkust, lapsepuhkust ja täiendavat lapsepuhkust antakse töötajale avalduse alusel seaduses ettenähtud korras ja ulatuses. 
5. Õppepuhkus 
5.1 Töötajal on õigus saada tasemekoolituses osalemiseks õppepuhkust.
5.2 Õppepuhkust antakse täiskasvanute koolituse seaduses ettenähtud tingimustel.
5.3 Õppepuhkuse saamiseks esitab töötaja tõendi õppimise kohta. Õppepuhkuse saamiseks esitab töötaja tööandjale kirjaliku avalduse 14 kalendripäeva enne õppepuhkuse kasutamist. 
6. Erijuhud puhkuste andmisel, tagasikutsumisel.
6.1 Tööandja peab arvestama, et kui tema katkestab töötaja puhkuse, sätestab seadus tööandjale kohustuse hüvitada töötajale puhkuse katkestamisest või edasilükkamisest tulenevad kulud (töölepingu seadus § 69 lõige 5 lause 2). 
( Näiteks tuleb tööandjal kaaluda, kas ta on valmis kinni maksma töötaja ostetud puhkusereisi, mida too puhkuse katkestamise tõttu kasutada ei saa, või on odavam leida teine töötaja, kes täidab ootamatult tekkinud vajalikud tööülesanded.)

