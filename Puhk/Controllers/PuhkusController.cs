﻿using Puhk.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Collections.Generic;
using System;

namespace Puhk.Controllers
{
    public class PuhkusController : Controller
    {
        private PuhkContext db = new PuhkContext();

        // GET: Puhkus
        //public ActionResult Index()
        //{
        //    //var puhkused = db.Puhkused.Include(p => p.Töötaja);
        //    //return View(puhkused.ToList());
            
        //}

        // GET: Puhkus/Details/5
        public ActionResult Details(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Puhkus puhkus = db.Puhkused.Find(id);
                if (puhkus == null)
                {
                    return HttpNotFound();
                }
                return View(puhkus);
            }
            return RedirectToAction("Index", "Home");


            
        }
        
        // GET: Puhkus/Create
        public ActionResult Create(int? id)
        {
            //ViewBag.TöötajaId = new SelectList(db.Töötajad, "Id", "Nimi");
            //if (id == null)
            //{
            //    // siis mitte ei anna viga vaid paned siia asemele sisseloginud isiku ID

            //    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Töötaja t = db.Töötajad.Find(id.Value);
            //if (t == null)
            //{
            //    return HttpNotFound();
            //}
            ////kas see id on selle töötaja id ja kui ei ole, kas töötaja on
            //// sama osakonna juhataja - siis select listis SELLE osakonna töötajad
            //// HR
            //// juku - nendel kahel select listis kõik töötajad
            //return View();

            if (Request.IsAuthenticated)
            {
                ViewBag.TöötajaId = new SelectList(db.Töötajad, "Id", "Nimi", id);
                ViewBag.KasutajaVoli = true;                                                                   // false ja ture on valesti... vist
                return View();
            }
            return RedirectToAction("Index", "Home");
            
        }

        // POST: Puhkus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PuhkuseTüüp,AlgusKP,LõppKP,TöötajaId,Staatus")] Puhkus puhkus)
        {

            if (Request.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    if (Töötaja.SisseloginudTöötaja.TöötajaRoll == Töötaja.Roll.Töötaja)
                    {
                        Töötaja t = Töötaja.SisseloginudTöötaja;
                        if (t != null)
                        {
                            puhkus.TöötajaId = t.Id;
                            db.Puhkused.Add(puhkus);
                            db.SaveChanges();
                        }
                        return RedirectToAction("MinuPuhkus");
                    }
                    else
                    {
                        db.Puhkused.Add(puhkus);
                        db.SaveChanges();
                        return RedirectToAction("Koik");
                    }

                }

                ViewBag.TöötajaId = new SelectList(db.Töötajad, "Id", "Nimi", puhkus.TöötajaId);
                return View(puhkus);
            }
            return RedirectToAction("Index", "Home");
          
        }

        // GET: Puhkus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Puhkus puhkus = db.Puhkused.Find(id);
                if (puhkus == null)
                {
                    return HttpNotFound();
                }
                ViewBag.TöötajaId = new SelectList(db.Töötajad, "Id", "Nimi", puhkus.TöötajaId);
                return View(puhkus);
            }
            return RedirectToAction("Index", "Home");

           
        }

        // POST: Puhkus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PuhkuseTüüp,AlgusKP,LõppKP,TöötajaId,Staatus")] Puhkus puhkus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(puhkus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TöötajaId = new SelectList(db.Töötajad, "Id", "Nimi", puhkus.TöötajaId);
            return View(puhkus);
        }

        public ActionResult ArvutaPuhkusepäevadKokku()  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        {
            var töötaja = Töötaja.SisseloginudTöötaja;
            var käesolevAasta = DateTime.Now.Year;
            var puhkused = db.Puhkused.Where(p => p.TöötajaId == töötaja.Id).ToList();
            double kõikPuhkusePäevadKokku = 0;
            foreach ( var puhkus in puhkused)
            {
                if(puhkus.AlgusKP.Year == käesolevAasta)
                {
                    var ühePuhkusePäevadKokku = (puhkus.LõppKP - puhkus.AlgusKP).TotalDays;
                    kõikPuhkusePäevadKokku += ühePuhkusePäevadKokku;
                }
            }
            return View(kõikPuhkusePäevadKokku);
        }

        public ActionResult KinnitaPuhkus(int id)
        {
                Puhkus puhkus = db.Puhkused.Find(id);
                if (puhkus != null)
                {
                    puhkus.Staatus = "Kinnitatud";
                    db.SaveChanges();
                }
                if (Töötaja.SisseloginudTöötaja.TöötajaRoll == Töötaja.Roll.Osakonnajuht)
                {
                    return RedirectToAction("OsakonnaPuhkused");
                }
                else
                {
                    return RedirectToAction("Koik");
                }
        }
        public ActionResult TühistaPuhkus(int id)
        {
            Puhkus puhkus = db.Puhkused.Find(id);
            if (puhkus != null)
            {
                puhkus.Staatus = "Tühistatud";
                db.SaveChanges();
            }
            if (Töötaja.SisseloginudTöötaja.TöötajaRoll == Töötaja.Roll.Osakonnajuht)
            {
                return RedirectToAction("OsakonnaPuhkused");
            }
            else
            {
                return RedirectToAction("Koik");
            }
        }

        public ActionResult MinuPuhkus()
        {
            if (Request.IsAuthenticated)
            {
                var töötaja = Töötaja.SisseloginudTöötaja;
                var puhkused = db.Puhkused.Where(x => x.TöötajaId == töötaja.Id).ToList();
                return View(puhkused);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult OsakonnaPuhkused(string search, string option)
        {
            Töötaja t = Töötaja.SisseloginudTöötaja;
            var osakond = t.OsakondId;
            var puhkused = db.Puhkused.Where(p => p.Töötaja.OsakondId == osakond).ToList();
            var model = new OsakonnaPuhkusedViewModel
            {
                //Nimetus = osakond.Nimetus,
                Puhkused = puhkused
            };

            if (option == "Nimi")                                                                               // see jupp teeb töötajate otsingut
            {

                model.Puhkused = t.Osakond.Töötajad.Where(x => x.Nimi.ToLower().StartsWith(search.ToLower()) || search == null).SelectMany(x => x.Puhkused).ToList();
            }
            else if (option == "Isikukood")
            {
                model.Puhkused = t.Osakond.Töötajad.Where(x => x.Isikukood.StartsWith(search) || search == null).SelectMany(x => x.Puhkused).ToList();
            }
            
            return View(model);
        }

        public ActionResult Koik(string option, string search)
        {
            var osakondadePuhkused = new List<OsakonnaPuhkusedViewModel>();
            var osakonnad = db.Osakonnad.Include(o => o.Töötajad).ToList();
            foreach (var osakond in osakonnad)
            {
                var osakonnaNimi = osakond.Nimetus;
                var puhkused = osakond.Töötajad.SelectMany(t => t.Puhkused);
                var osakonnaPuhkused = new OsakonnaPuhkusedViewModel
                {
                    Nimetus = osakonnaNimi,
                    Puhkused = puhkused.ToList()
                };
                osakondadePuhkused.Add(osakonnaPuhkused);
            }
            return View(osakondadePuhkused);
        }

        public ActionResult Index(int? id)
        {
            Töötaja töötaja = Töötaja.ByKontakt(User.Identity.Name);
            ViewBag.Töötaja = töötaja;
            ViewBag.Osakonnad = db.Osakonnad.ToList();
            if (id.HasValue && id.Value == 0)
            {
                var puhkused = db.Puhkused.Where(x => x.TöötajaId == töötaja.Id).ToList();
                return View("Index", puhkused);
            }
            if (töötaja.TöötajaRoll == Töötaja.Roll.Firmajuht || töötaja.TöötajaRoll == Töötaja.Roll.Personalijuht)
            {
                if (id != null)
                {
                    var puhkused = db.Puhkused.Where(x => x.Töötaja.OsakondId == id).ToList();
                    return View(puhkused);
                }
                else
                {
                    var puhkused = db.Puhkused.ToList();
                    return View(puhkused);
                }
            }
            else
            {
                var puhkused = db.Puhkused.Where(x => x.Töötaja.OsakondId == töötaja.OsakondId).ToList();
                return View(puhkused);
            }

        }
        // GET: Puhkus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Puhkus puhkus = db.Puhkused.Find(id);
            if (puhkus == null)
            {
                return HttpNotFound();
            }
            return View(puhkus);
        }

        // POST: Puhkus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Puhkus puhkus = db.Puhkused.Find(id);
            db.Puhkused.Remove(puhkus);
            db.SaveChanges();
            return RedirectToAction("Koik");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }

}
