﻿using Puhk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Puhk.Controllers
{

    //[Authorize]
    public class HomeController : Controller
    {
        private PuhkContext db = new PuhkContext();

        public ActionResult Index()
        {
            List<Töötaja> TöötajaList = db.Töötajad.ToList();

            ////Töötaja t = db.Töötaja.Where(x => x.Kontakt == User.Identity.Name).Take(1).SingleOrDefault();
            Töötaja t = Töötaja.ByKontakt(User.Identity.Name);

            if (t != null && t.TöötajaRoll == Töötaja.Roll.Töötaja)
            {
                return RedirectToRoute("Töötaja", new { id = t.Id });
            }

            if (t != null && t.TöötajaRoll == Töötaja.Roll.Osakonnajuht)
            {
                return RedirectToRoute("Osakond", new { id = t.Id });
            }

            if (t != null && t.TöötajaRoll == Töötaja.Roll.Firmajuht)
            {
                return RedirectToRoute("Firmajuht", new { id = t.Id });
            }

            if (t != null && t.TöötajaRoll == Töötaja.Roll.Personalijuht)
            {
                return RedirectToRoute("Personalijuht", new { id = t.Id });
            }

            return View(TöötajaList);
        }




        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}