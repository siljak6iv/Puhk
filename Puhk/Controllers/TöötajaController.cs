﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Puhk;
using Puhk.Models;

namespace Puhk.Models
{
    partial class Töötaja
    {
        static PuhkContext db = new PuhkContext();

        public static Töötaja ByKontakt(string email)
        {
            
            return db.Töötajad.Where(x => x.Kontakt == email).Take(1).SingleOrDefault();
        }

        public static Töötaja SisseloginudTöötaja
        {
            get
            {
                var töötaja = HttpContext.Current.Session["Töötaja"] as Töötaja 
                              ?? ByKontakt(HttpContext.Current.User.Identity.Name);

                if (töötaja == null)
                {
                    HttpContext.Current.Response.Redirect("~/"); //kui töötaja pole sisse loginud läheb avalehele
                }

                return töötaja;
            }
            private set => HttpContext.Current.Session["Töötaja"] = value;
        }

        public static bool LogiTöötajaSisse(string email)
        {
            var töötaja = Töötaja.ByKontakt(email);
            SisseloginudTöötaja = töötaja;
            return töötaja != null;
        }
    }

}

namespace Puhk.Controllers
{
    public class TöötajaController : Controller
    {
        private PuhkContext db = new PuhkContext();

        // GET: Töötaja
        public ActionResult Index(string option, string search)
        {
            if (option == "Nimi")                                                                               // see jupp teeb töötajate otsingut
            {
                return View(db.Töötajad.Where(x => x.Nimi.StartsWith(search) || search == null).ToList());
            }
            else if (option == "Isikukood")
            {
                return View(db.Töötajad.Where(x => x.Isikukood.StartsWith(search) || search == null).ToList());
            }
            else if (option == "TöötajaRoll")
            {
                return View(db.Töötajad.Where(x => x.TöötajaRoll.ToString() == search || search == null).ToList());
            }

            if (Request.IsAuthenticated && Töötaja.SisseloginudTöötaja.TöötajaRoll != Töötaja.Roll.Töötaja)
            {
                if (!Request.IsAuthenticated) return RedirectToAction("Index", "Home");
                Töötaja k = Töötaja.ByKontakt(User.Identity.Name);
                if (k == null) return RedirectToAction("Index", "Home");
                return View(db.Töötajad.ToList());
            }
            return RedirectToAction("Index", "Home");

            
        }

        public ActionResult OsakonnaTöötajad()
        {
            if (Request.IsAuthenticated && Töötaja.SisseloginudTöötaja.TöötajaRoll != Töötaja.Roll.Töötaja)
            {
                Töötaja töötaja = Töötaja.SisseloginudTöötaja;
                var osakond = töötaja.OsakondId;
                var osakonnaTöötajad = db.Töötajad.Where(p => p.OsakondId == osakond).ToList();
                return View(osakonnaTöötajad);
            }
            return RedirectToAction("Index", "Home");
            
        }

        // GET: Töötaja/Details/5
        public ActionResult Details(int? id)
        {
            Töötaja töötaja = db.Töötajad.Find(id);
            if (Request.IsAuthenticated && Töötaja.SisseloginudTöötaja.Id == töötaja.Id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                if (töötaja == null)
                {
                    return HttpNotFound();
                }
                return View(töötaja);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Töötaja/Create
      
        public ActionResult Create()
        {
            if (Request.IsAuthenticated && Töötaja.SisseloginudTöötaja.TöötajaRoll != Töötaja.Roll.Töötaja)
            {
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
   
        // POST: Töötaja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Isikukood,Nimi,Kontakt,TööAlgusKP,TööLõppKP,OsakondId,TöötajaRoll")] Töötaja töötaja)
        {
            if (ModelState.IsValid)
            {
                db.Töötajad.Add(töötaja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(töötaja);
        }

        // GET: Töötaja/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Töötaja töötaja = db.Töötajad.Find(id);
            if (töötaja == null)
            {
                return HttpNotFound();
            }
            return View(töötaja);
        }

        // POST: Töötaja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Isikukood,Nimi,Kontakt,TööAlgusKP,TööLõppKP,OsakondId,TöötajaRoll")] Töötaja töötaja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(töötaja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(töötaja);
        }

        // GET: Töötaja/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Töötaja töötaja = db.Töötajad.Find(id);
            if (töötaja == null)
            {
                return HttpNotFound();
            }
            return View(töötaja);
        }

        // POST: Töötaja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Töötaja töötaja = db.Töötajad.Find(id);
            db.Töötajad.Remove(töötaja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
