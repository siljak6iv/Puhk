﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Puhk.Models;
using Puhk;

namespace Puhk.Controllers
{
    public class OsakondController : Controller
    {
        private PuhkContext db = new PuhkContext();

        // GET: Osakond
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(db.Osakonnad.ToList());
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Osakond/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Osakond osakond = db.Osakonnad.Find(id);
            if (osakond == null)
            {
                return HttpNotFound();
            }
            return View(osakond);
        }

        // GET: Osakond/Create
        public ActionResult Create()
        {
            if (Request.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        // POST: Osakond/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimetus")] Osakond osakond)
        {
            if (ModelState.IsValid)
            {
                db.Osakonnad.Add(osakond);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(osakond);
        }

        // GET: Osakond/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Osakond osakond = db.Osakonnad.Find(id);
                if (osakond == null)
                {
                    return HttpNotFound();
                }
                return View(osakond);
            }
            return RedirectToAction("Index", "Home");
            
        }

        // POST: Osakond/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimetus")] Osakond osakond)
        {
            if (ModelState.IsValid)
            {
                db.Entry(osakond).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(osakond);
        }

        // GET: Osakond/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Osakond osakond = db.Osakonnad.Find(id);
                if (osakond == null)
                {
                    return HttpNotFound();
                }
                return View(osakond);
            }
            return RedirectToAction("Index", "Home");
        }

        // POST: Osakond/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Osakond osakond = db.Osakonnad.Find(id);
            db.Osakonnad.Remove(osakond);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
