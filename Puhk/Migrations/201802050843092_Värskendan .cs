namespace Puhk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Värskendan : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Töötaja", "Osakond_Id", "dbo.Osakonds");
            DropIndex("dbo.Töötaja", new[] { "Osakond_Id" });
            RenameColumn(table: "dbo.Töötaja", name: "Osakond_Id", newName: "OsakondId");
            AlterColumn("dbo.Töötaja", "OsakondId", c => c.Int(nullable: false));
            CreateIndex("dbo.Töötaja", "OsakondId");
            AddForeignKey("dbo.Töötaja", "OsakondId", "dbo.Osakonds", "Id", cascadeDelete: true);
            DropColumn("dbo.Töötaja", "OsakonnaID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Töötaja", "OsakonnaID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Töötaja", "OsakondId", "dbo.Osakonds");
            DropIndex("dbo.Töötaja", new[] { "OsakondId" });
            AlterColumn("dbo.Töötaja", "OsakondId", c => c.Int());
            RenameColumn(table: "dbo.Töötaja", name: "OsakondId", newName: "Osakond_Id");
            CreateIndex("dbo.Töötaja", "Osakond_Id");
            AddForeignKey("dbo.Töötaja", "Osakond_Id", "dbo.Osakonds", "Id");
        }
    }
}
