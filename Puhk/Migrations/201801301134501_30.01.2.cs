namespace Puhk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _30012 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Puhkus", "Staatus", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Puhkus", "Staatus");
        }
    }
}
