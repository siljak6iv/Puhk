namespace Puhk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kaheksas : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Töötaja", new[] { "OsakondId" });
            CreateIndex("dbo.Töötaja", "OsakondID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Töötaja", new[] { "OsakondID" });
            CreateIndex("dbo.Töötaja", "OsakondId");
        }
    }
}
