namespace Puhk.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Algne : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Osakonds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nimetus = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Töötaja",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Isikukood = c.Int(nullable: false),
                        Nimi = c.String(),
                        Kontakt = c.String(),
                        TööAlgusKP = c.DateTime(nullable: false),
                        TööLõppKP = c.DateTime(),
                        OsakondID = c.Int(nullable: false),
                        TöötajaRoll = c.Int(nullable: false),
                        Osakond_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Osakonds", t => t.Osakond_Id)
                .Index(t => t.Osakond_Id);
            
            CreateTable(
                "dbo.Puhkus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PuhkuseTüüp = c.Int(nullable: false),
                        AlgusKP = c.DateTime(nullable: false),
                        LõppKP = c.DateTime(nullable: false),
                        TöötajaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Töötaja", t => t.TöötajaId, cascadeDelete: true)
                .Index(t => t.TöötajaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Puhkus", "TöötajaId", "dbo.Töötaja");
            DropForeignKey("dbo.Töötaja", "Osakond_Id", "dbo.Osakonds");
            DropIndex("dbo.Puhkus", new[] { "TöötajaId" });
            DropIndex("dbo.Töötaja", new[] { "Osakond_Id" });
            DropTable("dbo.Puhkus");
            DropTable("dbo.Töötaja");
            DropTable("dbo.Osakonds");
        }
    }
}
