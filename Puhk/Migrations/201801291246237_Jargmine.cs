namespace Puhk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Jargmine : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Osakonds", "Nimetus", c => c.String(nullable: false));
            AlterColumn("dbo.Töötaja", "Isikukood", c => c.String(nullable: false, maxLength: 11));
            AlterColumn("dbo.Töötaja", "Nimi", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Töötaja", "Kontakt", c => c.String(nullable: false, maxLength: 40));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Töötaja", "Kontakt", c => c.String());
            AlterColumn("dbo.Töötaja", "Nimi", c => c.String());
            AlterColumn("dbo.Töötaja", "Isikukood", c => c.Int(nullable: false));
            AlterColumn("dbo.Osakonds", "Nimetus", c => c.String());
        }
    }
}
