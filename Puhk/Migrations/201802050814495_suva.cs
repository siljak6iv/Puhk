namespace Puhk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class suva : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Töötaja", "OsakondID", "dbo.Osakonds");
            DropIndex("dbo.Töötaja", new[] { "OsakondID" });
            RenameColumn(table: "dbo.Töötaja", name: "OsakondID", newName: "Osakond_Id");
            AddColumn("dbo.Töötaja", "OsakonnaID", c => c.Int(nullable: false));
            AlterColumn("dbo.Töötaja", "Osakond_Id", c => c.Int());
            CreateIndex("dbo.Töötaja", "Osakond_Id");
            AddForeignKey("dbo.Töötaja", "Osakond_Id", "dbo.Osakonds", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Töötaja", "Osakond_Id", "dbo.Osakonds");
            DropIndex("dbo.Töötaja", new[] { "Osakond_Id" });
            AlterColumn("dbo.Töötaja", "Osakond_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Töötaja", "OsakonnaID");
            RenameColumn(table: "dbo.Töötaja", name: "Osakond_Id", newName: "OsakondID");
            CreateIndex("dbo.Töötaja", "OsakondID");
            AddForeignKey("dbo.Töötaja", "OsakondID", "dbo.Osakonds", "Id", cascadeDelete: true);
        }
    }
}
